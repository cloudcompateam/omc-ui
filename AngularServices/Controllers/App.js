
/// <reference path="../../Scripts/angular.js" />
/// <reference path="../../Scripts/angular-datatables.buttons.js" />
var application = angular.module('iApp', []);
//var application = angular.module('iApp',[]);
/*application.run(function ($rootScope, DTOptionsBuilder) {
    $rootScope.vm = {};
    $rootScope.vm.dtOptions = DTOptionsBuilder.newOptions().withOption('scrollX','100%')
                          				  .withButtons([
                                          {
                                              extend: 'copy',
                                              text: '<i class="fa fa-files-o"></i> Copy',
                                              titleAttr: 'Copy'
                                          },
                                          {
                                              extend: 'print',
                                              text: '<i class="fa fa-print" aria-hidden="true"></i> Print',
                                              titleAttr: 'Print'
                                          },
                                          {
                                              extend: 'excel',
                                              text: '<i class="fa fa-file-text-o"></i> Excel',
                                              titleAttr: 'Excel'
                                          },
                                          {
                                              extend: 'pdf',
                                              text: '<i class="fa fa-file-pdf-o"></i> Pdf',
                                              titleAttr: 'PDF'
                                          }
                          				  ]
                                    );

});

application.controller('LogoutController', function ($location, $scope, $window) {
    $scope.logout = function () {
        $window.localStorage.clear();
        window.location.assign("login.html");
    }

});*/


application.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit(attr.onFinishRender);
                });
            }
        }
    }

    function ngRepeatFinished(ngRepeatFinishedEvent) {
        var table = $('#sample_11');
        var oTable = table.dataTable();



    };

});



application.directive('numberFormat', ['$filter', '$parse', function ($filter, $parse) {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelController) {

            var decimals = $parse(attrs.decimals)(scope);

            ngModelController.$parsers.push(function (data) {
                // Attempt to convert user input into a numeric type to store
                // as the model value (otherwise it will be stored as a string)
                // NOTE: Return undefined to indicate that a parse error has occurred
                //       (i.e. bad user input)
                var parsed = parseFloat(data);
                return !isNaN(parsed) ? parsed : undefined;
            });

            ngModelController.$formatters.push(function (data) {
                //convert data from model format to view format
                return $filter('number')(data, decimals); //converted
            });

            element.bind('focus', function () {
                element.val(ngModelController.$modelValue);
            });

            element.bind('blur', function () {
                // Apply formatting on the stored model value for display
                var formatted = $filter('number')(ngModelController.$modelValue, decimals);
                element.val(formatted);
            });
        }
    }
}]);










