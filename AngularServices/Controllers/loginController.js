/// <reference path="App.js" />
application.controller('loginController', function ($scope, $log, $rootScope, $window) {


    
    // toastr.options.timeOut = 1500; // 1.5s
    $scope.year = new Date().getFullYear();
    $scope.form = {};
    $scope.form.mailBox = 'adminID';
    $scope.form.pass = 'admin';
 
    $scope.loginmodal = function(){
     $('#signuptext').addClass("hidden");
     $('#divname').addClass("hidden");
 
     $('#registerbtn').addClass("hidden");
     $('#loginbtn').removeClass("hidden");
 }
 
 $scope.signupmodal = function(){
    $('#signintext').addClass("hidden");
    $('#signuptext').removeClass("hidden");
    $('#divname').removeClass("hidden");
 
    $('#loginbtn').addClass("hidden");
    $('#registerbtn').removeClass("hidden");
 }
 
 $scope.login = function () {
       //  $scope.email = "damilareoyebanji@gmail.com";
        // $scope.password = "1759";
        var logindata = {"email": $scope.email,"password": $scope.password};
        console.log(JSON.stringify(logindata))
        $.ajax({
           url:"http://192.168.72.148:80/users/login",
           type:"POST",
           data:JSON.stringify(logindata),
           contentType:"application/json; charset=utf-8",
           dataType:"json",
           success: function(data,status){
             console.log(data)
             console.log(data.apiKey)
             if(data.apiKey != " " || data.apiKey != undefined){
                 localStorage['ID'] = data.apiKey; 
                 localStorage['email'] = data.email 
                 $("#logout").removeClass("hidden")
                 $("#history").removeClass("hidden") 
 
                 $("#login").addClass("hidden")
                 $("#signup").addClass("hidden")
 
                 $(".username-hide-mobile").removeClass("hidden") 
                 document.getElementById('modal_close').click()
                 toastr.success('Welcome '+ localStorage["email"]);
                 $(".username-hide-mobile").html(localStorage['email']);
 
                 $scope.historyfunc();
 
             }else if(data.apiKey == " " || data.apiKey == undefined){
               toastr.error("Invalid email and password");
           }else{
              toastr.error("Invalid email and password");
          }
      }
  })
    }
 
    $scope.register = function () {
     var registerdata = {"name": $scope.fullname, "email": $scope.email,"password": $scope.password};
     console.log(JSON.stringify(registerdata))
     $.ajax({
       url:"http://192.168.72.148:80/users",
       type:"POST",
       data:JSON.stringify(registerdata),
       contentType:"application/json; charset=utf-8",
       dataType:"json",
       success: function(data,status){
         console.log(data)
         console.log(data.apiKey)
         if(data.apiKey != " " || data.apiKey != undefined){
             toastr.success('registration successful'+ data.email);
             document.getElementById('modal_close').click()
         }else if(data.apiKey == " " || data.apiKey == undefined){
           toastr.error("Invalid email and password");
       }else{
          toastr.error("Invalid email and password");
      }
  }
 })
 }
 
 
 $scope.logout = function(){
     $("#logout").addClass("hidden")
     $("#history").addClass("hidden") 
 
     $("#login").removeClass("hidden")
     $("#signup").removeClass("hidden")
 
     $(".username-hide-mobile").addClass("hidden")   
     toastr.success("Logging Off....");           
     localStorage.clear();
 }
 
 var file;
 var fd = new FormData();
 var selectedformat;
 $('input[type="file"]').change(function(e){
     file = e.target.files[0];
             //fd.append('inputFile', file);
             console.log(file.name);
         });
 $("select.input-lg").change(function(){
  selectedformat = $(this).children("option:selected").val();
  $scope.outputFormat_text = selectedformat;
  console.log($scope.outputFormat_text);
 
 });
 
 $('#convert-buttons').click(function(e){
     e.preventDefault();
 });
 $scope.wrapperfunc = function(){   
     document.getElementById('file').click();
 } 
 
 
 $('#convert-butto').click(function(){
     alert("Jquery First");
     document.getElementById('myForm').action = 'http://192.168.72.148/users/'+localStorage['ID']+'/convert'
 })
 
 
 $scope.convert = function (){
 
     $.ajax({
       url:"http://192.168.72.148/users/"+localStorage['ID']+"/convert",
       type:"POST",
       data:fd,
       cache: false,
       contentType: false, 
       processData: false, 
       success: function(data,status){
         //console.log(data);
         var blob = new Blob([data]);
         download(blob, "download." +selectedformat, "");
 
     }
 })
 
 }
 
 $scope.historyfunc = function(){
     if(localStorage['ID'] != undefined){
        $.ajax({
           url:"http://192.168.72.148/users/"+localStorage['ID']+"/transactions",
           type:"GET",
           success: function(data,status){
             $scope.userhistory = data ;  
             console.log($scope.userhistory);
         }
     })
    }
 
 }
 
 
 $scope.dropdownarr = [
         {"name": "PNG","value" : "png"},
         {"name": "JPEG","value" : "JPEG"},
         {"name": "animated GIF","value" : "animated GIF"},
         {"name": "OGG","value" : "ogg"},
         {"name": "Raw Video","value" : "raw video"},
         {"name": "MPEG-1 video","value" : "MPEG-1 video"},
         {"name": "MP3","value" : "MP3"}
 ]
 
 
 });