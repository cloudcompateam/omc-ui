var CHOOSE_TEXT = "Convert file(s) to:";
var SEPERATOR   = " ";
var IMAGE_TEXT  = "-- Image formats --";
var MUSIC_TEXT  = "-- Music formats --";
var DOC_TEXT    = "-- Doc formats --";
var VIDEO_TEXT  = "-- Video formats --";
var EBOOK_TEXT  = "-- E-Book formats --";
var COMPRESSED_TEXT  = "-- Other formats --";
var VIDEO_SEPERATOR  = "-- Preset Options --";
var DOCUMENT_SEPERATOR  = "-- Document Programs --";
var EBOOK_SEPERATOR  = "-- eBook Devices --";
var toArray = new Array(CHOOSE_TEXT,
SEPERATOR,
IMAGE_TEXT,
"bmp",
"dwg",
"dxf",
"emf",
"eps",
"gif",
"ico",
"jpg",
"pcx",
"png",
"tga",
"tiff",
"wbmp",
"webp",
"wmf",
DOC_TEXT,
"csv",
"doc",
"docx",
"html",
"key",
"key.zip",
"numbers",
"numbers.zip",
"odp",
"ods",
"odt",
"pages",
"pages.zip",
"pcx",
"pdf",
"pps",
"ppt",
"pptx",
"ps",
"pub",
"rtf",
"txt",
"vsd",
"wks",
"wpd",
"wps",
"xlr",
"xls",
"xlsx",
"xml",
MUSIC_TEXT,
"aac",
"ac3",
"flac",
"m4a",
"mmf",
"mp3",
"ogg",
"ra",
"ram",
"wav",
"wma",
VIDEO_TEXT,
"3gp",
"3g2",
"asf",
"avi",
"flv",
"gvi",
"ipad",
"iphone",
"ipod",
"m4v",
"mod",
"mov",
"mp4",
"mpg",
"ogg",
"rm",
"rmvb",
"vob",
"wmv",
EBOOK_TEXT,
"azw",
"cbz",
"cbr",
"cbc",
"chm",
"epub",
"fb2",
"lit",
"lrf",
"mobi",
"prc",
"pdb",
"pml",
"rb",
"tcr",
COMPRESSED_TEXT,
"7z",
"tar.bz2",
"cab",
"lzh",
"rar",
"tar",
"tar.gz",
"yz1",
"zip");
azw = new Array(CHOOSE_TEXT,"epub","fb2","oeb","lit","lrf","mobi","mp3","pdf","pdb","pml","prc","rb","tcr","txt");
cbz = new Array(CHOOSE_TEXT,"epub","fb2","oeb","lit","lrf","mobi","mp3","pdf","pdb","pml","prc","rb","tcr","txt");
cbr = new Array(CHOOSE_TEXT,"epub","fb2","oeb","lit","lrf","mobi","mp3","pdf","pdb","pml","prc","rb","tcr","txt");
cbc = new Array(CHOOSE_TEXT,"epub","fb2","oeb","lit","lrf","mobi","mp3","pdf","pdb","pml","prc","rb","tcr","txt");
chm = new Array(CHOOSE_TEXT,"epub","fb2","oeb","lit","lrf","mobi","mp3","pdf","pdb","pml","prc","rb","tcr","txt");
epub = new Array(CHOOSE_TEXT,"fb2","oeb","lit","lrf","mobi","mp3","pdf","pdb","pml","prc","rb","tcr","txt");
fb2 = new Array(CHOOSE_TEXT,"epub","oeb","lit","lrf","mobi","mp3","pdf","pdb","pml","prc","rb","tcr","txt");
lit = new Array(CHOOSE_TEXT,"epub","fb2","oeb","lrf","mobi","mp3","pdf","pdb","pml","prc","rb","tcr","txt");
lrf = new Array(CHOOSE_TEXT,"epub","fb2","oeb","lit","mobi","mp3","pdf","pdb","pml","prc","rb","tcr","txt");
mobi = new Array(CHOOSE_TEXT,"epub","fb2","oeb","lit","lrf","mp3","pdf","pdb","pml","prc","rb","tcr","txt");
prc = new Array(CHOOSE_TEXT,"epub","fb2","oeb","lit","lrf","mobi","mp3","pdf","pdb","pml","rb","tcr","txt");
pdb = new Array(CHOOSE_TEXT,"epub","fb2","oeb","lit","lrf","mobi","mp3","pdf","pml","prc","rb","tcr","txt");
pml = new Array(CHOOSE_TEXT,"epub","fb2","oeb","lit","lrf","mobi","mp3","pdf","pdb","prc","rb","tcr","txt");
rb = new Array(CHOOSE_TEXT,"epub","fb2","oeb","lit","lrf","mobi","mp3","pdf","pdb","pml","prc","tcr","txt");
tcr = new Array(CHOOSE_TEXT,"epub","fb2","oeb","lit","lrf","mobi","mp3","pdf","pdb","pml","prc","rb","txt");
zip = new Array(CHOOSE_TEXT,"7z","tar.bz2","cab","lzh","tar","tar.gz","yz1");
bz2 = new Array(CHOOSE_TEXT,"7z","cab","lzh","tar","tar.gz","yz1","zip");
cab = new Array(CHOOSE_TEXT,"7z","tar.bz2","lzh","tar","tar.gz","yz1","zip");
rar = new Array(CHOOSE_TEXT,"7z","cab","tar.bz2","lzh","tar","tar.gz","yz1","zip");
lzh = new Array(CHOOSE_TEXT,"7z","tar.bz2","cab","tar","tar.gz","yz1","zip");
tar = new Array(CHOOSE_TEXT,"7z","tar.bz2","cab","lzh","tar.gz","yz1","zip");
gz = new Array(CHOOSE_TEXT,"7z","tar.bz2","cab","lzh","tar","yz1","zip");
yz1 = new Array(CHOOSE_TEXT,"7z","tar.bz2","cab","lzh","tar","tar.gz","zip");
dwg = new Array(CHOOSE_TEXT,"bmp","gif","jpg","pdf","png","tiff");
dxf = new Array(CHOOSE_TEXT,"bmp","gif","jpg","pdf","png","tiff");
jpg = new Array(CHOOSE_TEXT,"bmp","doc","gif","ico","pcx","pdf","png","ps","tga","thumbnail","tiff","wbmp","webp");
gif = new Array(CHOOSE_TEXT,"bmp","jpg","pcx","pdf","png","ps","tga","thumbnail","tiff","wbmp","webp");
bmp = new Array(CHOOSE_TEXT,"gif","ico","jpg","pcx","pdf","png","ps","tga","thumbnail","tiff","wbmp","webp");
png = new Array(CHOOSE_TEXT,"bmp","gif","ico","jpg","pcx","pdf","ps","tga","thumbnail","tiff","wbmp","webp");
wmf = new Array(CHOOSE_TEXT,"bmp","gif","ico","jpg","pcx","pdf","png","ps","tga","thumbnail","tiff","wbmp","webp");
eps = new Array(CHOOSE_TEXT,"bmp","gif","ico","jpg","pcx","pdf","png","ps","tga","thumbnail","tiff","wbmp");
tga = new Array(CHOOSE_TEXT,"bmp","gif","ico","jpg","pcx","pdf","png","ps","thumbnail","tiff","wbmp","webp");
pcx = new Array(CHOOSE_TEXT,"bmp","gif","ico","jpg","pdf","png","ps","tga","thumbnail","tiff","wbmp","webp");
emf = new Array(CHOOSE_TEXT,"bmp","gif","ico","jpg","pcx","pdf","png","ps","tga","thumbnail","tiff","wbmp","webp");
webp = new Array(CHOOSE_TEXT,"bmp","gif","jpg","pcx","pdf","png","tga","tiff","wbmp");
wbmp = new Array(CHOOSE_TEXT,"bmp","gif","ico","jpg","pcx","pdf","png","ps","tga","thumbnail","tiff","webp");
mpp = new Array(CHOOSE_TEXT,"bmp","gif","jpg","pdf","png","tiff","xls");
tiff = new Array(CHOOSE_TEXT,"bmp","gif","ico","jpg","pcx","pdf","png","ps","tga","thumbnail","wbmp","webp");
ps = new Array(CHOOSE_TEXT,"bmp","gif","jpg","pdf","tiff","thumbnail");
eps = new Array(CHOOSE_TEXT,"bmp","gif","ico","jpg","pcx","pdf","png","ps","tga","thumbnail","tiff","wbmp");
ram = new Array(CHOOSE_TEXT,"aac","ac3","flac","m4a","mp3","mp4","ogg","wav","wma");
ra = new Array(CHOOSE_TEXT,"aac","ac3","flac","m4a","mp3","mp4","ogg","wav","wma");
aac = new Array(CHOOSE_TEXT,"ac3","flac","m4a","mp3","mp4","ogg","wav","wma");
mp3 = new Array(CHOOSE_TEXT,"aac","ac3","flac","m4a","mp4","ogg","wav","wma");
m4a = new Array(CHOOSE_TEXT,"aac","ac3","flac","mp3","mp4","ogg","wav","wma");
wav = new Array(CHOOSE_TEXT,"aac","ac3","flac","m4a","mp3","mp4","ogg","wma");
flac = new Array(CHOOSE_TEXT,"aac","ac3","m4a","mp3","ogg","wav");
wma = new Array(CHOOSE_TEXT,"aac","m4a","mp3","ogg","mp4","wav");
ac3 = new Array(CHOOSE_TEXT,"aac","m4a","mp3","ogg","wav");
asf = new Array(CHOOSE_TEXT,"3g2","3gp","aac","ac3","avi","flac","flv","ipad","mov","mp3","mp4","mpg","ogg","wav","webm","wmv");
mov = new Array(CHOOSE_TEXT,"3gp","3g2","aac","ac3","avi","flac","flv","gif","mp3","mp4","mpg","ogg","wav","webm","wmv",SEPERATOR,VIDEO_SEPERATOR,"Apple TV","Blackberry","DVD","FLV for web use","Galaxy Tab","HTC Desire","HTC Dream","HTC Droid Eris","HTC Hero","HTC Magic","iPad","iPhone4","iPhone","iPod Touch","Kindle Fire","Motorola Cliq","Motorola DEXT","Motorola DROID","Motorola Xoom","myTouch","Nexus One","Nokia","PS3","PSP","Samsung Behold II","Vimeo","Wii","Xbox360","YouTube","Zune");
mp4 = new Array(CHOOSE_TEXT,"3gp","3g2","aac","ac3","avi","flac","flv","gif","ipad","iphone","ipod","mov","mp3","mpg","ogg","wav","webm","wmv",SEPERATOR,VIDEO_SEPERATOR,"Apple TV","Blackberry","DVD","FLV for web use","Galaxy Tab","HTC Desire","HTC Dream","HTC Droid Eris","HTC Hero","HTC Magic","iPad","iPhone4","iPhone","iPod Touch","Kindle Fire","Motorola Cliq","Motorola DEXT","Motorola DROID","Motorola Xoom","myTouch","Nexus One","Nokia","PS3","PSP","Samsung Behold II","Vimeo","Wii","Xbox360","YouTube","Zune");
m4v = new Array(CHOOSE_TEXT,"3gp","3g2","aac","ac3","avi","flac","flv","ipad","iphone","ipod","mov","mp3","mpg","ogg","wav","webm","wmv");
mpg = new Array(CHOOSE_TEXT,"3gp","3g2","aac","ac3","avi","flac","flv","ipad","iphone","ipod","mov","mp3","mp4","ogg","wav","webm","wmv",SEPERATOR,VIDEO_SEPERATOR,"Apple TV","Blackberry","DVD","FLV for web use","Galaxy Tab","HTC Desire","HTC Dream","HTC Droid Eris","HTC Hero","HTC Magic","iPad","iPhone4","iPhone","iPod Touch","Kindle Fire","Motorola Cliq","Motorola DEXT","Motorola DROID","Motorola Xoom","myTouch","Nexus One","Nokia","PS3","PSP","Samsung Behold II","Vimeo","Wii","Xbox360","YouTube","Zune");
avi = new Array(CHOOSE_TEXT,"3gp","3g2","aac","ac3","flac","flv","gif","mp3","mov","mp4","mpg","ogg","wav","webm","wmv",SEPERATOR,VIDEO_SEPERATOR,"Apple TV","Blackberry","DVD","FLV for web use","Galaxy Tab","HTC Desire","HTC Dream","HTC Droid Eris","HTC Hero","HTC Magic","iPad","iPhone4","iPhone","iPod Touch","Kindle Fire","Motorola Cliq","Motorola DEXT","Motorola DROID","Motorola Xoom","myTouch","Nexus One","Nokia","PS3","PSP","Samsung Behold II","Vimeo","Wii","Xbox360","YouTube","Zune");
flv = new Array(CHOOSE_TEXT,"3gp","3g2","aac","ac3","avi","flac","flv","gif","ipad","iphone","ipod","mp3","mov","mp4","mpg","ogg","wav","webm","wmv",SEPERATOR,VIDEO_SEPERATOR,"Apple TV","Blackberry","DVD","FLV for web use","Galaxy Tab","HTC Desire","HTC Dream","HTC Droid Eris","HTC Hero","HTC Magic","iPad","iPhone4","iPhone","iPod Touch","Kindle Fire","Motorola Cliq","Motorola DEXT","Motorola DROID","Motorola Xoom","myTouch","Nexus One","Nokia","PS3","PSP","Samsung Behold II","Vimeo","Wii","Xbox360","YouTube","Zune");
mod = new Array(CHOOSE_TEXT,"3gp","3g2","aac","ac3","avi","flac","flv","ipad","iphone","ipod","mp3","mov","mp4","mpg","ogg","wav","webm","wmv");
wmv = new Array(CHOOSE_TEXT,"3gp","3g2","aac","ac3","avi","flac","flv","mov","mp3","mp4","mpg","ogg","wav","webm",SEPERATOR,VIDEO_SEPERATOR,"Apple TV","Blackberry","DVD","FLV for web use","Galaxy Tab","HTC Desire","HTC Dream","HTC Droid Eris","HTC Hero","HTC Magic","iPad","iPhone4","iPhone","iPod Touch","Kindle Fire","Motorola Cliq","Motorola DEXT","Motorola DROID","Motorola Xoom","myTouch","Nexus One","Nokia","PS3","PSP","Samsung Behold II","Vimeo","Wii","Xbox360","YouTube","Zune");
rm = new Array(CHOOSE_TEXT,"3gp","3g2","aac","ac3","avi","flac","flv","ipad","iphone","ipod","mov","mp3","mp4","mpg","ogg","wav","webm","wmv");
rmvb = new Array(CHOOSE_TEXT,"3gp","3g2","aac","ac3","avi","flac","flv","ipad","iphone","ipod","mov","mp3","mp4","mpg","ogg","wav","webm","wmv");
ts = new Array(CHOOSE_TEXT,"3gp","3g2","aac","ac3","avi","flac","flv","ipad","iphone","ipod","mov","mp3","mp4","mpg","ogg","wav","webm","wmv");
vob = new Array(CHOOSE_TEXT,"3gp","3g2","aac","ac3","avi","flac","flv","ipad","iphone","ipod","mp3","mov","mp4","mpg","ogg","wav","webm","wmv");
gvi = new Array(CHOOSE_TEXT,"3g2","aac","avi","flv","ipad","iphone","ipod","mpg","mov","mp3","mp4","ogg","wav","webm","wmv");
doc = new Array(CHOOSE_TEXT,"azw3","bmp","docx","epub","fb2","gif","html","html4","html5","jpg","lit","lrf","mobi","mp3","odt","oeb","pcx","pdf","pml","png","ps","rb","tcr","tiff","thumbnail","txt");
docx = new Array(CHOOSE_TEXT,"azw3","bmp","docx","epub","fb2","gif","html","html4","html5","jpg","lit","lrf","mobi","mp3","odt","oeb","pcx","pdf","pml","png","ps","rb","tcr","tiff","thumbnail","txt");
pages = new Array(CHOOSE_TEXT,"doc", "pdf", "rtf", "txt");
key = new Array(CHOOSE_TEXT,"html", "ipod", "jpg", "mov", "pdf", "png", "ppt", "tiff");
numbers = new Array(CHOOSE_TEXT,"csv", "pdf", "xls");
wps = new Array(CHOOSE_TEXT,"doc","mp3","odt","pcx","pdf","png","ps","txt");
wpd = new Array(CHOOSE_TEXT,"doc","mp3","odt","pcx","pdf","png","ps","txt");
vsd = new Array(CHOOSE_TEXT,"bmp","gif","html","jpg","pdf","png","tif");
rtf = new Array(CHOOSE_TEXT,"doc","docx","html","pcx","pdf","png","ps");
pdf = new Array(CHOOSE_TEXT,"bmp","doc","docx","epub","fb2","gif","html","jpg","lit","lrf","mobi","mp3","odt","oeb","pcx","pdb","pml","png","prc","ps","rb","rtf","tcr","tiff","thumbnail","txt","xls");
pub = new Array(CHOOSE_TEXT,"doc","docx","html","mp3","odt","pcx","pdf","png","ps","rtf","txt");
xls = new Array(CHOOSE_TEXT,"bmp","csv","doc","gif","html","jpg","mdb","ods","pdf","png","rtf","tiff","xml","xlsx");
xlr = new Array(CHOOSE_TEXT,"csv","doc","html","mdb","ods","pdf","rtf","xls","xlsx","xml");
wks = new Array(CHOOSE_TEXT,"csv","doc","html","mdb","ods","pdf","rtf","xml","xls","xlsx");
xlsx = new Array(CHOOSE_TEXT,"bmp","csv","doc","gif","html","jpg","mdb","ods","pdf","png","rtf","tiff","xml","xls");
csv = new Array(CHOOSE_TEXT,"doc","html","mdb","ods","pdf","rtf","xls","xlsx","xml");
ppt = new Array(CHOOSE_TEXT,"bmp","gif","doc","docx","html","odp","odt","pcx","pdf","png","ppt (1997-2003)","pptx","ps","rtf","swf","tiff","txt");
pps = new Array(CHOOSE_TEXT,"bmp","gif","doc","docx","html","odp","odt","pcx","pdf","png","ppt","pptx","ps","rtf","swf","tiff","txt");
pptx = new Array(CHOOSE_TEXT,"bmp","gif","doc","docx","html","odp","odt","pcx","pdf","png","ppt","pptx","ps","rtf","swf","tiff","txt");
odt = new Array(CHOOSE_TEXT,"doc","docx","html","mp3","pdf","png","ps","txt");
txt = new Array(CHOOSE_TEXT,"epub","mp3");
ods = new Array(CHOOSE_TEXT,"csv","doc","html","mdb","pdf","rtf","xml","xls","xlsx");
odp = new Array(CHOOSE_TEXT,"html","pcx","pdf","png","ppt","ps","swf");
threegtwo = new Array(CHOOSE_TEXT,"3gp","aac","ac3","avi","flac","flv","ipad","ipod","mov","mp3","mp4","mpg","ogg","wav","webm","wmv");
threegp = new Array(CHOOSE_TEXT,"3g2","aac","ac3","avi","flac","flv","mov","mp3","mp4","mpg","ogg","wav","webm","wmv",SEPERATOR,VIDEO_SEPERATOR,"Apple TV","Blackberry","DVD","FLV for web use","Galaxy Tab","HTC Desire","HTC Dream","HTC Droid Eris","HTC Hero","HTC Magic","iPad","iPhone4","iPhone","iPod Touch","Kindle Fire","Motorola Cliq","Motorola DEXT","Motorola DROID","Motorola Xoom","myTouch","Nexus One","Nokia","PS3","PSP","Samsung Behold II","Vimeo","Wii","Xbox360","YouTube","Zune");
pageszip = new Array(CHOOSE_TEXT,"doc", "pdf", "rtf", "txt");
keyzip = new Array(CHOOSE_TEXT,"html", "ipod", "jpg", "mov", "pdf", "png", "ppt", "tiff");
numberszip = new Array(CHOOSE_TEXT,"csv", "pdf", "xls");
Choose = new Array(" ");
var fuzzyMatchArray = new Array ("numbers.zip","numberszip","key.zip","keyzip","pages.zip","pageszip","jpeg","jpg","mpeg","mpg","3gp","threegp","3g2","threegtwo","tif","tiff","ppt(1997-2003)","ppt");
