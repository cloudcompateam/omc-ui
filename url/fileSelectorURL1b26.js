var lastURLValue="";
var ALREADY_ADDED_FILE="Error: File has already been added\n\nPlease select another one or hit the convert button";
var NO_EXTENSION="Error: File has no extension\n\nPlease select one that has and try again";
var MISMATCHED_EXTENSION="Error: When selecting multiple files, ensure that they all have the same extension";
var UNSUPPORTED_EXTENSION="Filetype conversion is not currently supported for \"";
var NO_FILES_SELECTED="Error: Upload at least one file before trying to convert";
var CONVERT_CANCELLED_MESSAGE="Please modify your information or file selections and try again";
var STALLED_ERROR_MSG="Error: Sorry, there has been a problem uploading your file - the upload has stalled.\n\nPlease try again.";
var EXCEEDED_MAX_FILES="Error: For URL uploads you can only upload a single file at a time for conversion\n\nOpen another browser window to upload another file from a URL or wait until this upload is complete."
var NOTOEXT_ERROR_MSG="Please select a file format to convert to";
var INVALID_URL="Error: The URL you entered is invalid\n\nPlease check the URL and try again.";
var INVALID_EMAIL="Please provide a valid email address, or untick the notification checkbox (in Step 3)";
var FILELIST_HEADER_STYLE="fileListHeader";
var FILELIST_UPLOAD_STYLE="body";
var FILELIST_TEXT="";
var REMOVE_BUTTON_TEXT="Remove File"
var STATUS_SCRIPT="/cgi-bin/getStatusURL.pl";
var CONVERSION_SCRIPT="/cgi-bin/handleFileUploadURL.pl";
var BACKGROUND_COLOUR_DARK="#CCCCCC";
var BACKGROUND_COLOUR_LIGHT="#F2F2F2";
var toExt="";
var toEmail="";
var noLogErrorCount=0;
var formKeyErrorCount=0;
var badXMLErrorCount=0;
var badHTTPRespCount=0;
var currentUploadedTotalSizeKB=0;
var titleOrig=document.title;
var globalFilename="";
var lastPercentComplete=0;
var percentCompleteSameCount=0;
var tcshu="";
var globalFileId="";
var uploadCompleteSession = "";

function fileArrayAdd(fileName) {
    fileName=untaintString(fileName);
    fileArray.push(fileName);
}
function fileArrayRemove(fileName) {
    fileName=untaintString(fileName);
    for (i=0; i < fileArray.length; i+=1) {
        if(fileArray[i] == fileName) {
            fileArray[i]="";
        }
    }
}
function getNextFileFromFileArray() {
    var fileName="null";
    for (i=0; i < fileArray.length; i+=1) {
        if(fileArray[i] != "") {
            return fileArray[i];
        }
    }
    return fileName;
}   
function fileArraySearch(fileName) {
    fileName=untaintString(fileName);
    var found=false;
    for (i=0; i < fileArray.length; i+=1) {
        if(fileArray[i] == fileName) {
            found=true;
            break;
        }
    }
    return found;
}
function fileExtensionCheck(fileExtension) {
    var match=false;
    if(fileExtension != "zip")fileExtension=fileExtension.replace("zip",".zip")
    for (i=0; i < fileArray.length; i+=1) {
        if(fileArray[i].toUpperCase().indexOf(fileExtension.toUpperCase()) != -1) {
            match=true;
            break;
        }
    }
    return match;
}
function checkValidURL(fileURL) {
    if((fileURL.indexOf("http://") == 0 || fileURL.indexOf("https://") == 0) && fileURL.indexOf("<") == -1 && fileURL.indexOf(">") == -1 && fileURL.indexOf("&#60;") == -1 && fileURL.indexOf("&#62;") == -1 && fileURL.indexOf("&lt;") == -1 && fileURL.indexOf("&gt;") == -1) return true;
    else return false;
}
function getFileExtension(fileName) {
    var extension="";
    fileName=untaintString(fileName);
    if(fileName.indexOf("youtu.be/") != -1 || fileName.indexOf("youtube.com/") != -1 || fileName.indexOf("youtube.com/watch") != -1 || (fileName.indexOf("video.google") != -1 && fileName.indexOf("/videoplay") != -1) || fileName.indexOf("putfile.com") != -1 || fileName.indexOf("break.com/") != -1 || fileName.indexOf("dailymotion.com/cluster") != -1 || fileName.indexOf("metacafe.com/watch") != -1 || fileName.indexOf("spike.com/video") != -1 || fileName.indexOf("blip.tv/file/") != -1 || fileName.indexOf("vids.myspace.com/") != -1 || fileName.indexOf("myspace.com/video") != -1 || fileName.indexOf("spikedhumor.com/articles") != -1 || fileName.indexOf("teachertube.com/viewVideo") != -1 || fileName.indexOf("tinypic.com/player.php?v=") != -1 || fileName.indexOf("godtube.com/watch") != -1) {
        extension="flv";
    } else if (fileName.indexOf("www.apple.com/trailers/") != -1 || fileName.indexOf("revver.com/video") != -1) {
        extension="mov";
    } else if(fileName.indexOf(".pages.zip")!=-1){
        extension="pages.zip";
    }else if(fileName.indexOf(".numbers.zip")!=-1){
        extension="numbers.zip";
    }else if(fileName.indexOf(".key.zip")!=-1){
        extension="key.zip";
    } else {
        var extensionIndex=fileName.lastIndexOf('.');
        if (extensionIndex != -1) {
            extension=fileName.slice(fileName.lastIndexOf('.')+1);
        }
    }
    return extension;
}
function isFileSupported(fileExtension) {
    var retn;
    var isSupported=false;
    eval("try { if(isNaN(retn=eval(fileExtension))) retn=-1; } catch (e) { retn=-2; }");    
    if(retn == -1) isSupported=true;
    return isSupported;
}
function doFuzzyMatch(fileExtension) {
    for(var i=0; i<fuzzyMatchArray.length; i+=2) {
        if (fuzzyMatchArray[i] == fileExtension) {
            fileExtension=fuzzyMatchArray[i+1];
            break;
        }
    }
    return fileExtension;
}
function resetFileForm(uploadTableCell) { 
    window.location.reload(false);
}

function hideSelectList() {
    document.getElementById("toExtensionSel").style.visibility="hidden";
}
function showSelectList() {
    document.getElementById("toExtensionSel").style.visibility="visible";
}
function FileList( list_target ){
    this.list_target=list_target;
    count=0;
    this.id=0;
    fileArray=new Array();
    var headerDiv1=document.getElementById("headerDiv1");
    var headerDiv2=document.getElementById("headerDiv2");
    var fileInputElementParent=document.getElementById("fileInputElementParent");
    var fileTable=document.getElementById("fileTable");
    var fileCounter=document.getElementById("fileCounter");
    document.getElementById("uid").value=clientUID;
    this.addElement=function( element, addURLButtonElement ){        
        if( element.tagName == 'INPUT' && element.type == 'text' ){    
            element.name='file_' + this.id++; 
            element.multi_selector=this;
            addURLButtonElement.onclick=function(){
                element.value=trim(element.value);
                if (!checkValidURL(element.value)) {
                    alert(INVALID_URL);                    
                    return false;
                }
                var fileExtension=getFileExtension(element.value).toLowerCase();
                if (fileExtension == "") {
                    alert(NO_EXTENSION);                    
                    return false;
                }
                fileExtension=doFuzzyMatch(fileExtension);
                if (!isFileSupported(fileExtension)) {
                    alert(UNSUPPORTED_EXTENSION + fileExtension + "\"");                    
                    return false;
                }
                if (fileArraySearch(element.value)) { 
                    alert(ALREADY_ADDED_FILE);                    
                    return false;
                }        
                if (count != 0 && fileExtensionCheck(fileExtension) != true) {                     
                    alert(MISMATCHED_EXTENSION);
                    return false;
                }
                var toFormat = document.fileForm.toExtensionSel.value;
                if (toFormat === '' || toFormat === '-') {
                    alert(NOTOEXT_ERROR_MSG);
                    return false;
                }
                if ($('#notify-checkbox').prop('checked') && !validateEmail(document.fileForm.toEmail.value)) {
                    alert(INVALID_EMAIL);
                    return false;
                }
                element.multi_selector.addListRow( element );
            };
        }
    };
    this.addListRow=function( element ){
        fileArrayAdd(element.value);     
        fileCounter.value=count;
        count++;
        var new_row_button=document.createElement( 'input' );
        new_row_button.type='button';
        new_row_button.id='removeButton';
        new_row_button.value=REMOVE_BUTTON_TEXT;
        tr0=fileTable.insertRow(0);
        tr0.id=untaintString(element.value) + "tr0";
        tr1=fileTable.insertRow(1);
        tr2=fileTable.insertRow(2);
        tr2.id=untaintString(element.value) + "tr2";
        var backgroundImageColor="dk";
        var backgroundColor=BACKGROUND_COLOUR_DARK;
        tr1.style.backgroundColor=backgroundColor; 
        if (fileTable.rows[3] != null) {
            if (fileTable.rows[3].childNodes[0].style.backgroundImage.indexOf("corner_dk") != -1) {
                backgroundImageColor="lt";
                backgroundColor=BACKGROUND_COLOUR_LIGHT;
                tr1.style.backgroundColor=backgroundColor;
            }
        }
        td1=tr0.insertCell(0);
        td1.style.backgroundImage="url(/images/corner_" + backgroundImageColor + "_tl.gif)";
        td1.innerHTML="";
        td1.style.width=7 + "px";
        ;
        td2=tr0.insertCell(1);
        td2.style.backgroundColor=backgroundColor; 
        td3=tr0.insertCell(2);
        td3.style.backgroundColor=backgroundColor; 
        td4=tr0.insertCell(3);
        td4.style.backgroundColor=backgroundColor; 
        td5=tr0.insertCell(4);
        td5.style.backgroundColor=backgroundColor; 
        td6=tr0.insertCell(5);
        td6.style.backgroundImage="url(/images/corner_" + backgroundImageColor + "_tr.gif)";
        td6.innerHTML="";
        td6.style.width=7 + "px";
        td7=tr1.insertCell(0);
        td8=tr1.insertCell(1);
        if(element.value.length < 100) {
            td8.className="nowrapping";
        }
        td8.innerHTML=shortenURL(element.value);
        td9=tr1.insertCell(2);
        td9.className="nowrapping";
        td9.innerHTML="&nbsp;&nbsp;&nbsp;";
        td9.appendChild(new_row_button);
        td10=tr1.insertCell(3);
        td10.innerHTML="&nbsp;&nbsp;";
        td11=tr1.insertCell(4);
        td11.width="100%";
        td11.id=untaintString(element.value) + "uploadTableCell";
        td12=tr1.insertCell(5);
        td13=tr2.insertCell(0);
        td13.style.backgroundImage="url(/images/corner_" + backgroundImageColor + "_bl.gif)";
        td13.innerHTML="";
        td13.style.width=7 + "px";
        td14=tr2.insertCell(1);
        td14.style.backgroundColor=backgroundColor; 
        td15=tr2.insertCell(2);
        td15.style.backgroundColor=backgroundColor; 
        td16=tr2.insertCell(3);
        td16.style.backgroundColor=backgroundColor; 
        td17=tr2.insertCell(4);
        td17.style.backgroundColor=backgroundColor;
        td18=tr2.insertCell(5);
        td18.style.backgroundImage="url(/images/corner_" + backgroundImageColor + "_br.gif)";
        td18.innerHTML="";
        td18.style.width=7 + "px";
        new_row_button.onclick= function(){
            var rowRefNum=this.parentNode.parentNode.rowIndex;
            var filename=lengthenURL(this.parentNode.parentNode.cells[1].innerHTML);
            if (rowRefNum != fileTable.rows.length && rowRefNum != 1) {
                if ((fileTable.rows[rowRefNum].style.backgroundColor.indexOf("cccccc") == 1) || (fileTable.rows[rowRefNum].style.backgroundColor.indexOf("204") == 4)) {
                    convertRowsToLight(rowRefNum, fileTable);
                    convertRowsToDark(rowRefNum + 3, fileTable);
                } else {
                    convertRowsToDark(rowRefNum, fileTable);
                    convertRowsToLight(rowRefNum + 3, fileTable);
                }
            }
            fileArrayRemove(filename); 
            fileCounter.value=count;
            this.parentNode.parentNode.parentNode.removeChild( document.getElementById(untaintString(filename) + "tr0"));
            this.parentNode.parentNode.parentNode.removeChild( document.getElementById(untaintString(filename) + "tr2"));
            this.parentNode.parentNode.parentNode.removeChild( this.parentNode.parentNode );
            count--;
            if(count == 0) {
                resetToExtensionValues();
                lastURLValue="";
            }
            return false;
        };
    };
};
function changeFormExtensionValue(fileExtension) {
    var optionArray=eval(fileExtension);
    var toExtensionSel=document.fileForm.toExtensionSel;
    for(var i=0; i<optionArray.length; i++) {
        toExtensionSel.options.length=0;
        for(var j=0; j<optionArray.length; j++) {
            var toValue=optionArray[j];
            toExtensionSel.options[j]=new Option(toValue,toValue);
        }
    }
    toExtensionSel.value=toExtensionSel.options[1].value;
    currentFromValue=fileExtension;
}
function resetToExtensionValues() {
    var fh = new window.zamzarFormatHelper, dropdown = $('#toExtensionSel'), url, ext;

    fh.processDropdownOutputChoices(dropdown);
    // Bind to the change event of the URL text box
    $('#inputFile').change(function () {
        url = $(this).val();
        ext = fh.getUrlExtension(url);
        // Set the global current from value which is used when the form is submitted
        currentFromValue = ext;
        fh.processDropdownOutputChoices(dropdown, ['file.' + ext]);
    });

    // Set the list if the URL is already set
    url = $('#inputFile').val();
    if (url !== 'http://') {
        ext = fh.getUrlExtension(url);
        // Set the global current from value which is used when the form is submitted
        currentFromValue = ext;
        fh.processDropdownOutputChoices(dropdown, ['file.' + ext]);
    }
}
var http_request=null;
var xml=null;
function displayState() {
    try{
        if (http_request.readyState == 4) {
            if (http_request.status == 200) {
                if (typeof window.ActiveXObject == 'undefined') {
                    xml=http_request.responseXML;
                } else {            
                    xml=new ActiveXObject('Microsoft.XMLDOM');
                    xml.loadXML(http_request.responseText)
                }
                var statusType=xml.getElementsByTagName("type")[0].firstChild.nodeValue;
                var status=xml.getElementsByTagName("status")[0].firstChild.nodeValue;
                var uploadTableCell=document.getElementById(globalFilename+"uploadTableCell");
                var uploadStatusDiv=document.getElementById("uploadStatusDiv"); 
                var uploadTextDiv=document.getElementById("uploadText");
                if (noLogErrorCount > 40) {
                    alert(genErr("101-"+sn+"-"+tcs));
                    fileArray.reverse();
                    resetFileForm(uploadTableCell);
                    noLogErrorCount=0;
                    return false;
                }
                if (formKeyErrorCount > 100){
                    alert(genErr("102-"+sn+"-"+tcs));
                    fileArray.reverse(); 
                    resetFileForm(uploadTableCell);
                    formKeyErrorCount=0; 
                    return false;
                }
                if (statusType.indexOf("noLogError") != -1) {  
                    submitAjaxReq(STATUS_SCRIPT + "?action=&uid=" + globalFileId + "&fileName=" + escape(globalFilename) + "&type=upload&toExtensionSel=" + toExt + "&fileCounter=" + count + "&tcs=" + tcs);
                    noLogErrorCount ++;
                }
                if (statusType.indexOf("maxFileError") != -1) {
                    if (status.indexOf("0 MB") != -1) {
                        submitAjaxReq(STATUS_SCRIPT + "?action=&uid=" + globalFileId + "&fileName=" + escape(globalFilename) + "&type=upload&toExtensionSel=" + toExt + "&fileCounter=" + count + "&tcs=" + tcs);
                    } else {
                        alert(status + "\nPlease amend your file selections and try again.");
                        fileArray.reverse();
                        resetFileForm(uploadTableCell);
                    }
                }
                if (statusType.indexOf("maxSizeError") != -1) {  
                    alert(status + "\n\nTo convert bigger files signup for a Zamzar account (supports up to 2 GB)");
                    fileArray.reverse();
                    resetFileForm(uploadTableCell);
                }
                if (statusType.indexOf("formKeyError") != -1) {  
                    submitAjaxReq(STATUS_SCRIPT + "?action=&uid=" + globalFileId + "&fileName=" + escape(globalFilename) + "&type=upload&toExtensionSel=" + toExt + "&fileCounter=" + count + "&tcs=" + tcs);
                    formKeyErrorCount ++;
                }  
                if (statusType.indexOf("invalidURLError") != -1) { 
                    alert(status + "\n\nPlease check the URL you entered and try converting again.");
                    fileArray.reverse();
                    resetFileForm(uploadTableCell);
                }
                if (statusType.indexOf("zeroByteFileError") != -1) { 
                    alert(status + "\n\nPlease check the URL you entered and try converting again.");
                    fileArray.reverse();
                    resetFileForm(uploadTableCell);
                }
                if(statusType.indexOf("invalidEmailError") != -1){
                    alert(status + "\n\nPlease check the email address you entered and try converting again.");
                    fileArray.reverse();
                    resetFileForm(uploadTableCell);
                }
                if (statusType.indexOf("upload") != -1) {
                    var estimatedTimeRemaining=0;
                    var percentComplete=-1;
                    var us=xml.documentElement.getElementsByTagName("uploadSize")[0].firstChild;
                    var et=xml.documentElement.getElementsByTagName("elapsedTime")[0].firstChild;
                    var tus=xml.documentElement.getElementsByTagName("totalUploadSize")[0].firstChild;
                    if(us&&et&&tus){
                        var uploadSizeKB=bytesToKB(us.nodeValue);
                        var elapsedTime=et.nodeValue;
                        var totalUploadSizeKB=bytesToKB(tus.nodeValue);
                        var KBPerSecond=Math.ceil(uploadSizeKB/elapsedTime);
                        elapsedTime=secsToMins(elapsedTime);
                        var percentComplete=Math.ceil((100 / totalUploadSizeKB) * (currentUploadedTotalSizeKB + uploadSizeKB));
                        if(percentComplete==lastPercentComplete){
                            percentCompleteSameCount ++;
                        }else{
                            percentCompleteSameCount=0;
                        }
                        lastPercentComplete=percentComplete;
                        var estimatedTimeRemaining=secsToMins((totalUploadSizeKB - (currentUploadedTotalSizeKB + uploadSizeKB)) / KBPerSecond);
                        if (totalUploadSizeKB > 1024) var totalUploadSizeText=KBtoMB(totalUploadSizeKB) + " MB";
                        else var totalUploadSizeText=totalUploadSizeKB + " KB";
                        if ((currentUploadedTotalSizeKB + uploadSizeKB) > 1024) currentUploadedTotalSizeText=KBtoMB((currentUploadedTotalSizeKB + uploadSizeKB)) + " MB";
                        else var currentUploadedTotalSizeText=(currentUploadedTotalSizeKB + uploadSizeKB) + " KB";
                    }else{
                        badXMLErrorCount++;
                        if(badXMLErrorCount>30){
                            alert(genErr("103-"+sn+"-"+tcs));
                            fileArray.reverse();
                            resetFileForm(uploadTableCell);
                            badXMLErrorCount=0; 
                            return false;
                        }
                    }

                    if (percentComplete != -1) {
                        var progressBar = document.querySelector("#uploadProgress .progress-bar");
                        progressBar.style.width = "" + percentComplete + "%";
                        progressBar.innerHTML = "Uploading (" + percentComplete + "%)";
                    }

                    if (status.indexOf("complete") != -1) {
                        if (uploadSizeKB > 1024) uploadSizeText=KBtoMB(uploadSizeKB) + " MB";
                        else uploadSizeText=uploadSizeKB + " KB";
                        currentUploadedTotalSizeKB += uploadSizeKB;
                        uploadTableCell.innerHTML="<span class='nowrapping'>Upload complete</span><br/><span class='nowrapping'>" + uploadSizeText + " in " + elapsedTime;
                        fileArrayRemove(globalFilename);
                        fileName=getNextFileFromFileArray();
                        globalFilename=fileName;
                        tempFileName=fileName;
                        if (globalFilename.indexOf("null") == -1) {
                            uploadTableCell=document.getElementById(globalFilename + "uploadTableCell");
                            ;
                            uploadTableCell.innerHTML="Uploading&nbsp;&nbsp;";
                            if(percentCompleteSameCount<90){
                                submitAjaxReq(STATUS_SCRIPT + "?action=&uid=" + globalFileId + "&fileName=" + escape(globalFilename) + "&type=upload&toExtensionSel=" + toExt + "&fileCounter=" + count + "&tcs=" + tcs);
                            }else{
                                alert(STALLED_ERROR_MSG);
                                fileArray.reverse(); 
                                resetFileForm(uploadTableCell);
                                percentCompleteSameCount=0;
                                lastPercentComplete=0;
                                return false;
                            }
                        }else{
                            uploadCompleteSession = globalFileId.split("-");
                            window.setTimeout('window.location="/uploadComplete.php?convertFiles="+currentFromValue+"&to="+toExt+"&session="+uploadCompleteSession[0]+"&tcs="+tcs', 500);
                        }
                    }else {
                        if(percentCompleteSameCount<90){
                            submitAjaxReq(STATUS_SCRIPT + "?action=&uid=" + globalFileId + "&fileName=" + escape(globalFilename) + "&type=upload&toExtensionSel=" + toExt + "&fileCounter=" + count + "&tcs=" + tcs);
                        }else{
                            alert(STALLED_ERROR_MSG);
                            fileArray.reverse(); 
                            resetFileForm(uploadTableCell);
                            percentCompleteSameCount=0;
                            lastPercentComplete=0;
                            return false;
                        }
                    }
                }
            }else{
                badHTTPRespCount++;
                if(badHTTPRespCount>150){
                    alert(genErr("104-"+sn+"-"+tcs+"["+http_request.status+"]"));
                    fileArray.reverse();
                    uploadTableCell=document.getElementById(tempFileName + "uploadTableCell");
                    resetFileForm(uploadTableCell);
                    badHTTPRespCount=0; 
                    return false;
                }else{
                    submitAjaxReq(STATUS_SCRIPT + "?action=&uid=" + globalFileId + "&fileName=" + escape(globalFilename) + "&type=upload&toExtensionSel=" + toExt + "&fileCounter=" + count + "&tcs=" + tcs);
                }
            }
        }else{
        //Causes recursion error on IE (stack overflow)
        //submitAjaxReq(STATUS_SCRIPT + "?action=&uid=" + globalFileId + "&fileName=" + escape(globalFilename) + "&type=upload&toExtensionSel=" + toExt + "&fileCounter=" + count + "&tcs=" + tcs);
        }
    }catch(e){
        submitAjaxReq(STATUS_SCRIPT + "?action=&uid=" + globalFileId + "&fileName=" + escape(globalFilename) + "&type=upload&toExtensionSel=" + toExt + "&fileCounter=" + count + "&tcs=" + tcs);
    }
}
function submitAjaxReq(script) {
    if (typeof window.ActiveXObject != 'undefined') {
        try {
            http_request=new ActiveXObject("Msxml2.XMLHTTP");
        }catch(e){
            try {
                http_request=new ActiveXObject("Microsoft.XMLHTTP");
            }catch(e){}
        }   
        http_request.onreadystatechange=displayState;
    }else{
        http_request=new XMLHttpRequest();
        http_request.onload=displayState;
        if(http_request.overrideMimeType){
            http_request.overrideMimeType('text/xml');
        }
    }
    http_request.open( "GET", script, true );
    window.setTimeout('http_request.send(null)', 2000);
}
function handleFormSubmit(){
    var targetURL=lengthenURL(document.getElementById("fileTable").rows[1].cells[1].innerHTML);
    if(targetURL.indexOf("youtu.be/") != -1 || targetURL.indexOf("youtube.com/") != -1 || targetURL.indexOf("youtube.com/watch") != -1){
        window.location="error/";
    }else{ 
        var filetext="";
        if (count == 1) {
            fileText=" file";
        } else {
            fileText=" files";
        }
        if (count != 0) {
            // Check if we have an email address
            var emailMessage = "";
            toEmail=document.fileForm.toEmail.value;
            if(toEmail === "" || toEmail == undefined) {
                //toEmail = "notset"
            } else if ($('#notify-checkbox').prop('checked')) {
                emailMessage = confirmMsg + toEmail;
            }

            var PRE_CONVERT_MESSAGE="About to convert " +
            count + fileText + " from " +
            currentFromValue.toUpperCase() + " to " +
            document.fileForm.toExtensionSel.value.toUpperCase() +
            emailMessage +
            "\n\nPress \"OK\" to start or \"Cancel\" to amend your details";

            if (confirm(PRE_CONVERT_MESSAGE)) {
                toExt=document.fileForm.toExtensionSel.value;
                toEmail=document.fileForm.toEmail.value;

                // Generate the client ID to use
                var convertSession = new window.zamzarSession({
                    serverId: tcs
                });
                
                fileArray.reverse();
                var fileName=getNextFileFromFileArray();
                globalFilename=fileName;
                tempFileName=fileName;
                        
                convertSession.prepareConversion(fileName, toEmail, toExt, function (err, fileId) {
                    if (err !== null) {
                        alert(err);
                    }
                    else {
                        globalFileId = fileId;

                        var uploadTableCell=document.getElementById(globalFilename + "uploadTableCell");
                        uploadTableCell.innerHTML="<span class='"+FILELIST_UPLOAD_STYLE+"'>Uploading&nbsp;&nbsp;</span>";

                        document.getElementById("uploadProgress").style.display = "block";

                        submitAjaxReq(STATUS_SCRIPT + "?action=&uid=" + globalFileId + "&fileName=" + escape(globalFilename) + "&type=upload&toExtensionSel=" + toExt + "&fileCounter=" + count + "&tcs=" + tcs);
                        //window.setTimeout('disableForm(\'fileForm\')',500);

                        // Don't call this event handler again
                        document.fileForm.onsubmit = null;

                        // Extract data from form
                        var fileTable=document.getElementById("fileTable");
                        var fileURL = lengthenURL(fileTable.rows[1].cells[1].innerHTML);
                        fileURL = fileURL.replace(/&amp;/g, '&');
                        fileURL = escape(fileURL);

                        var params = "fileFromExt=" + currentFromValue + '&' +
                            "fileToExt=" + toExt + '&' +
                            "clientUID=" + fileId + '&' +
                            "toEmail=" + toEmail + '&' +
                            "fileCount=" + count + '&' +
                            "sessionID=" + sessionID + '&' +
                            "fk=" + fk + '&' +
                            "file_0=" + fileURL + '&' +
                            "tcs=" + "tcs"+tcs;

                        // Build the target URL
                        // Note: URL conversions are not stateless, because they occur on a single
                        // web server and no other web server has visibility into the upload
                        // progress. As such, we need to ensure that all requests relating to
                        // URL conversions are routed to the same server. This is achieved by
                        // including a tcsXX parameter in the query string of the URL. Our
                        // Apache config then routes the request to the appropriate server.
                        // Because our Apache config matches on the string ";tcsXX", we include
                        // an additional, unused query string value ("pad") to ensure that the
                        // URL is valid
                        var url = CONVERSION_SCRIPT + "?pad;" + "tcs"+tcs;

                        // Send the conversion to the backend processor
                        var xhr = new XMLHttpRequest();
                        xhr.open('POST', url);
                        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                        xhr.send(params);
                    }
                });
                
                

            } else {
                removeSelectedFile();
                alert(CONVERT_CANCELLED_MESSAGE);
            }
        } else {
            alert(NO_FILES_SELECTED);
        }    
    }
}
function untaintString(myString) {
    if(myString.indexOf("youtube.com/watch") != -1) {
        if(myString.indexOf("playnext") != -1) {
            var re=/.*&v=/i;
            myString=myString.replace(re,"");
        }
        if(myString.indexOf("%23!")!=-1) {
            var re=/.*\%23!v=/i;
            myString=myString.replace(re,"");
        }
        if(myString.indexOf("#%21")!=-1) {
            var re=/.*#\%21v=/i;
            myString=myString.replace(re,"");
        }
        if(myString.indexOf("%23%21")!=-1) {
            var re=/.*\%23\%21v=/i;
            myString=myString.replace(re,"");
        }
        if(myString.indexOf("#!v")!=-1) {
            var re=/.*#!v=/i;
            myString=myString.replace(re,"");
        }
        var re=/watch\?v=/i;
        myString=myString.replace(re,"");
        var re=/\//;
        myString=myString.replace(re,"");
        var re=/&.*/;
        myString=myString.replace(re,"");
        myString += ".flv"; 
    }
    if (myString.indexOf("youtu.be") != -1) {
        var re=/.*\//i;
        myString=myString.replace(re,"");
        myString += ".flv"; 
    }
    if (myString.indexOf("download.php") != -1) {
        myString = unescape(myString);
        var re=/.*downloadFileName=/i;
        myString=myString.replace(re,"");
    }
    if(myString.indexOf("video.google") != -1 && myString.indexOf("videoplay") != -1) {
        var re=/videoplay\?docid=-/i;
        myString=myString.replace(re,"");
        var re=/videoplay\?docid=/i;
        myString=myString.replace(re,"");
        var re=/\//;
        myString=myString.replace(re,"");
        var re=/&.*/;
        myString=myString.replace(re,"");
        myString += ".flv"; 
    }
    if(myString.indexOf("revver.com/video/") != -1) { 
        var re=/.*revver.com\/video/i;
        myString=myString.replace(re,"");
        var re=/\/\d+\//;
        myString=myString.replace(re,"");
        var re=/\//;
        myString=myString.replace(re,"");
        myString += ".mov"; 
    }  
    if(myString.indexOf("putfile.com/") != -1) {
        var re=/.*putfile.com\//i;
        myString=myString.replace(re,"");
        var re=/\//;
        myString=myString.replace(re,"");
        var re=/\?.*/;
        myString=myString.replace(re,"");
        myString += ".flv"; 
    }
    if(myString.indexOf("break.com/") != -1) { 
        var re=/^.*\//i;
        myString=myString.replace(re,"");
        var re=/\..*/;
        myString=myString.replace(re,"");
        var re=/\//;
        myString=myString.replace(re,"");
        myString += ".flv";
    }
    if(myString.indexOf("dailymotion.com/") != -1) { 
        var re=/\/1/;
        myString=myString.replace(re,"");
        var re=/.*dailymotion.com\/.*\/video\//i;
        myString=myString.replace(re,"");
        var re=/\//;
        myString=myString.replace(re,"");
        myString += ".flv"; 
    }
    if(myString.indexOf("www.metacafe.com/watch/") != -1) {        
        var re=/.*www.metacafe.com\/watch\//i;
        myString=myString.replace(re,"");
        var re=/[0-9]*\//g;
        myString=myString.replace(re,"");
        var re=/\//g;
        myString=myString.replace(re,"");
        myString += ".flv"; 
    }
    if(myString.indexOf("spike.com/") != -1) {
        var re=/.*spike.com\/video\//i;
        myString=myString.replace(re,"");
        var re=/.*\//;
        myString=myString.replace(re,"");
        var re=/\//g;
        myString=myString.replace(re,"");
        myString += ".flv"; 
    }
    if(myString.indexOf("blip.tv/") != -1) {
        var re=/.*blip.tv\/file\//i;
        myString=myString.replace(re,"");
        var re=/\//;
        myString=myString.replace(re,"");
        var re=/\?.*/;
        myString=myString.replace(re,"");
        myString += ".flv"; 
    }
    if(myString.indexOf("vids.myspace.com/") != -1 || myString.indexOf("myspace.com/video") != -1) {
        var re=/\/$/;
        myString=myString.replace(re,"");
        var re=/.*video\/.*\/=/i;
        myString=myString.replace(re,"");
        var re=/.*videoid=/i;
        myString=myString.replace(re,"");
        var re=/\//;
        myString=myString.replace(re,"");
        myString += ".flv"; 
    }
    if(myString.indexOf("www.apple.com/trailers/") != -1) {
        var re=/^.*trailers\//i;
        myString=myString.replace(re,"");
        var re=/\//g;
        myString=myString.replace(re,"-");
        var re=/.html/;
        myString=myString.replace(re,"");
        var re=/-$/g;
        myString=myString.replace(re,"");
        myString += ".mov"; 
    }
    if(myString.indexOf("spikedhumor.com/articles") != -1) {
        var re=/.*articles.*\//i;
        myString=myString.replace(re,"");
        var re=/\//;
        myString=myString.replace(re,"");
        var re=/.html/;
        myString=myString.replace(re,"");
        myString += ".flv"; 
    }
    if(myString.indexOf("teachertube.com/viewVideo.php") != -1) {
        var re=/.*teachertube.com\/viewVideo.php\?video_id=/i;
        myString=myString.replace(re,"");
        var re=/.*&title=/i;
        myString=myString.replace(re,"");
        myString += ".flv"; 
    } 
    if(myString.indexOf("tinypic.com/player.php?v=") != -1) {
        var re=/.*tinypic.com\/player.php\?v=/i;
        myString=myString.replace(re,"");
        var re=/\&.*/;
        myString=myString.replace(re,"");
        myString += ".flv"; 
    } 
    if(myString.indexOf("godtube.com/watch") != -1) {
        var re=/.*godtube.com\/watch\/\?v=/i;
        myString=myString.replace(re,"");
        myString += ".mp4"; 
    }
    var re=/^.*(\/|\\)/;
    myString=myString.replace(re,"");
    re=/[:,`\*\?\|\%<>'"�]/g;
    myString=myString.replace(re,"");
    re=/\+\;/g;
    myString=myString.replace(re," ");
    return myString;
}
function bytesToKB(bytes) {
    var kb=Math.ceil(bytes/1024);
    return kb;
}
function KBtoMB(kb) {
    var mb=kb/1024;
    if (mb.toFixed) {
        mb=mb.toFixed(1);
    } else {
        mb=Math.ceil(mb);
    }
    return mb;
}
function secsToMins(seconds) {
    seconds=Math.floor(seconds);
    var timeString=seconds + " secs";
    if (seconds >= 60) {
        var mins=Math.floor(seconds/60);
        seconds=seconds%60;
        timeString=mins + " mins " + seconds + " secs";
        if (mins == 1) {
            timeString=mins + " min " + seconds + " secs";
        }
        if (seconds == 1) {
            timeString=mins + " mins " + seconds + " sec";
        }
        if (mins == 1 && seconds == 1) {
            timeString=mins + " min " + seconds + " sec";
        }
    }
    return timeString;
}
function trim(string){
    string=string.replace(/^\s*|\s*$/g,'');
    return string.replace(/http\:\/\/http\:\/\//g,'http://');
}
function shortenURL(string) {
    if(string.length > 80) {
        string=string.insertAt(81,"<br>");
    }
    return string;
}
function lengthenURL(string) {
    string=string.replace(new RegExp("<br> ",'g'),"");
    string=string.replace(new RegExp("<BR> ",'g'),"");
    string=string.replace(new RegExp("<br>",'g'),"");
    string=string.replace(new RegExp("<BR>",'g'),"");
    return string;
}
String.prototype.insertAt=function(loc,strChunk){ 
    return (this.valueOf().substr(0,loc))+strChunk+(this.valueOf().substr(loc)) 
} 
function changeToListForURL() {
    var URLValue=document.getElementById('inputFile').value;
    if(lastURLValue != URLValue) {
        var fileExtension=getFileExtension(URLValue).toLowerCase();
        fileExtension=doFuzzyMatch(fileExtension);
        if (fileExtension!="" && isFileSupported(fileExtension)) {
            changeFormExtensionValue(fileExtension);
        } else { 
            resetToExtensionValues();
        }
        lastURLValue=URLValue;
    }
}
function toListContainsText(optionText) {
    var isOptionInArray=false;
    var toExtensionSel=document.fileForm.toExtensionSel.options;    
    for (var i=0; i<toExtensionSel.length; i++) {
        if (toExtensionSel[i].value.indexOf(optionText) != -1) {
            isOptionInArray=true;
        }        
    }    
    return isOptionInArray;
}
function removeSelectedFile() { 
    var removeButton=document.getElementById("removeButton");
    removeButton.disabled=false;
    removeButton.focus();
    removeButton.click();   
}
function genErr(msgNo){
    return "There has been an internal error processing your request (Error "+msgNo+")\n\nPlease try re-submitting it";
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}